require 'time'
require 'net/http'

module GitlabResponseTime
  class Measure
    class ServiceUnavailableException < StandardError; end
    NUM_SAMPLES = 6

    def initialize(uri:, time: Time, http: Net::HTTP, delay: 10)
      @uri = uri
      @time = time
      @http = http
      @delay = delay
    end

    def run
      total = 0
      NUM_SAMPLES.times do
        total += response_time
        sleep(@delay)
      end
      total / NUM_SAMPLES
    rescue StandardError => e
      raise ServiceUnavailableException, e.message
    end

    private

    def response_time
      before = @time.now
      @http.get(URI(@uri))
      after = @time.now
      after - before
    end
  end
end
