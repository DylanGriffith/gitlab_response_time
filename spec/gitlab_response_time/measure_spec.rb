require 'gitlab_response_time/measure'

RSpec.describe GitlabResponseTime::Measure do
  let(:time) { class_double(Time) }
  let(:http) { instance_double(Net::HTTP) }
  let(:duration) do
    GitlabResponseTime::Measure.new(uri: 'https://example.com/', time: time, http: http, delay: 0).run
  end

  before(:each) do
    allow(time).to receive(:now).and_return(
      Time.at(100), Time.at(300),
      Time.at(100), Time.at(200),
      Time.at(200), Time.at(500),
      Time.at(100), Time.at(300),
      Time.at(100), Time.at(200),
      Time.at(200), Time.at(500)
    ) # Average diff of 200
  end

  describe '#run' do
    context 'when successful' do
      before(:each) do
        allow(http).to receive(:get).with(URI('https://example.com/'))
      end

      it 'returns the average duration' do
        expect(duration).to eq(200)
      end
    end

    context 'when unsuccessful' do
      before(:each) do
        allow(http).to receive(:get).and_raise('Something bad happened')
      end

      it 'raises ServiceUnavailableException' do
        expect { duration }.to raise_error(GitlabResponseTime::Measure::ServiceUnavailableException)
      end
    end
  end
end
