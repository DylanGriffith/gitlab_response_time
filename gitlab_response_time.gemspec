lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab_response_time/version'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab_response_time'
  spec.version       = GitlabResponseTime::VERSION
  spec.authors       = ['Dylan Griffith']
  spec.email         = ['dyl.griffith@gmail.com']

  spec.summary       = 'CLI tool for checking response time of gitlab.com'
  spec.description   = ''
  spec.homepage      = 'https://gitlab.com/DylanGriffith/gitlab_response_time'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.51.0'
end
